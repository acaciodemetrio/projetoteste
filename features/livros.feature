#language: pt
Funcionalidade: Consultar livros

  Cenario: CT001 – Consultar livro com o menor preço
    Dado eu estou na pagina inicial da Amazon usando o link "https://www.amazon.com.br"
    Quando eu preencher o campo pesquisa com o valor "C++"
    E clicar no botao de pesquisa
    E selecionar o filtro "Livros"
    Entao devera ser apresentado uma lista de itens
    E selecionar o formato "Capa Dura"
    E devera ser validado o item mais barato na lista

  Cenario: CT002 – Consultar livro com o maior preço
    Dado eu estou na pagina inicial da Amazon usando o link "https://www.amazon.com.br"
    Quando eu preencher o campo pesquisa com o valor "Java"
    E clicar no botao de pesquisa
    E selecionar o filtro "Livros"
    Entao devera ser apresentado uma lista de itens
    E selecionar o formato "Capa Comum"
    E devera ser validado o item mais caro na lista

  Cenario: CT003 – Consultar livro Kindle com a melhor avaliação
    Dado eu estou na pagina inicial da Amazon usando o link "https://www.amazon.com.br"
    Quando eu preencher o campo pesquisa com o valor "Python"
    E clicar no botao de pesquisa
    E selecionar o filtro "Loja Kindle"
    Entao devera ser apresentado uma lista de itens
    E devera ser validado o item melhor avaliado na lista