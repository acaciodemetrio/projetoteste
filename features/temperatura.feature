#language: pt
Funcionalidade: Consultar temperatura em uma cidade

  Cenario: CT001 – Consultar a temperatura em uma cidade
  Dado eu estou na pagina inicial do Google usando o link "https://www.google.com.br"
  Quando eu preencher o campo de busca com o valor "temperatura Salvador"
  E dou enter no campo de busca
  Entao devera ser apresentada a temperatura da cidade
