package stepdefinition;

import br.com.link.asserts.Verifications;
import br.com.link.setup.ConfigFramework;
import br.com.link.setup.Drivers;
import br.com.link.setup.ExtentReports;
import br.com.link.view.Actions;
import br.com.link.view.ComboBoxActions;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import cucumber.api.java.pt.Dado;
import org.openqa.selenium.By;

public class BaseSteps extends ConfigFramework {

    @Dado("^eu estou na pagina inicial da Amazon usando o link \"([^\"]*)\"$")
    public void eu_estou_na_pagina_inicial_da_Amazon_usando_o_link(String url)  {
        // Write code here that turns the phrase above into concrete actions
        Drivers.loadApplication(getBrowser(), url, true);
    }

    @Quando("^eu preencher o campo pesquisa com o valor \"([^\"]*)\"$")
    public void eu_preencher_o_campo_pesquisa_com_o_valor(String item)  {
        // Write code here that turns the phrase above into concrete actions
        Actions.setText(getBrowser(), By.id("twotabsearchtextbox"), item, 5);
        ExtentReports.appendToReport(getBrowser(), true);
    }

    @Quando("^clicar no botao de pesquisa$")
    public void clicar_no_botao_de_pesquisa()  {
        // Write code here that turns the phrase above into concrete actions
        Actions.clickOnElement(getBrowser(), By.className("nav-input"), 5);
    }

    @Quando("^selecionar o filtro \"([^\"]*)\"$")
    public void selecionar_o_filtro(String filtro)  {
        // Write code here that turns the phrase above into concrete actions

        // Clicar no filtro "Livros"
        Verifications.verifyElementIsVisible(getBrowser(), By.xpath("//h4[contains(text(), '" + filtro + "')]"), 5, true);
        Actions.clickOnElement(getBrowser(), By.xpath("//h4[contains(text(), '" + filtro + "')]"), 5);
    }

    @Quando("^selecionar o formato \"([^\"]*)\"$")
    public void selecionar_o_formato(String formato)  {
        // Write code here that turns the phrase above into concrete actions

        // Clicar no formato "Capa Comum" ou "Capa Dura"
        Verifications.verifyElementIsVisible(getBrowser(), By.xpath("//span[contains(text(), '" + formato + "')]"), 5, true);
        Actions.clickOnElement(getBrowser(), By.xpath("//span[contains(text(), '" + formato + "')]"), 5);
    }

    @Entao("^devera ser apresentado uma lista de itens$")
    public void devera_ser_apresentado_uma_lista_de_itens()  {
        // Write code here that turns the phrase above into concrete actions
        Verifications.verifyElementIsVisible(getBrowser(), By.id("sort"), 5, false);
        ExtentReports.appendToReport(getBrowser(), true);
    }

    @Entao("^devera ser validado o item mais barato na lista$")
    public void devera_ser_validado_o_item_mais_barato_na_lista()  {
        // Write code here that turns the phrase above into concrete actions
        ComboBoxActions.selectComboOptionByIndex(getBrowser(), By.id("sort"), 2, 5);
        Verifications.verifyElementIsVisible(getBrowser(), By.id("sort"), 5, true);

        String valor, nome;

        nome = Actions.getText(getBrowser(), By.xpath("//h2[contains(@class, 'a-size-medium') and contains(@class, 's-inline') and contains(@class, 's-access-title') and contains(@class, 'a-text-normal')][1]"), 5);
        valor = Actions.getText(getBrowser(), By.xpath("//span[contains(@class, 'a-size-base') and contains(@class, 'a-color-price') and contains(@class, 'a-text-bold')][1]"), 5);
        System.out.println("----------------------------------------------------");
        System.out.println("Item: [ " + nome + " ]");
        System.out.println("O item de capa dura mais barato custa " + valor + ".");
        System.out.println("----------------------------------------------------");
    }

    @Entao("^devera ser validado o item mais caro na lista$")
    public void devera_ser_validado_o_item_mais_caro_na_lista()  {
        // Write code here that turns the phrase above into concrete actions
        ComboBoxActions.selectComboOptionByIndex(getBrowser(), By.id("sort"), 3, 5);
        Verifications.verifyElementIsVisible(getBrowser(), By.id("sort"), 5, true);

        String valor, nome;

        nome = Actions.getText(getBrowser(), By.xpath("//h2[contains(@class, 'a-size-medium') and contains(@class, 's-inline') and contains(@class, 's-access-title') and contains(@class, 'a-text-normal')][1]"), 5);
        valor = Actions.getText(getBrowser(), By.xpath("//span[contains(@class, 'a-size-base') and contains(@class, 'a-color-price') and contains(@class, 'a-text-bold')][1]"), 5);
        System.out.println("----------------------------------------------------");
        System.out.println("Item: [ " + nome + " ]");
        System.out.println("O item de capa comum mais caro custa " + valor + ".");
        System.out.println("----------------------------------------------------");
    }

    @Entao("^devera ser validado o item melhor avaliado na lista$")
    public void devera_ser_validado_o_item_melhor_avaliado_na_lista()  {
        // Write code here that turns the phrase above into concrete actions
        ComboBoxActions.selectComboOptionByIndex(getBrowser(), By.id("sort"), 4, 5);
        Verifications.verifyElementIsVisible(getBrowser(), By.id("sort"), 5, true);

        String estrelas, nome;

        nome = Actions.getText(getBrowser(), By.xpath("//h2[contains(@class, 'a-size-medium') and contains(@class, 's-inline') and contains(@class, 's-access-title') and contains(@class, 'a-text-normal')][1]"), 5);
        estrelas = Actions.getText(getBrowser(), By.xpath("//a[contains(@class, 'a-size-small') and contains(@class, 'a-link-normal') and contains(@class, 'a-text-normal')][1]"), 5);
        System.out.println("----------------------------------------------------");
        System.out.println("Item: [ " + nome + " ]");
        System.out.println("O item da Kindle melhor avaliado possui " + estrelas + " estrela(s).");
        System.out.println("----------------------------------------------------");
    }

    @Dado("^eu estou na pagina inicial do Google usando o link \"([^\"]*)\"$")
    public void eu_estou_na_pagina_inicial_do_Google_usando_o_link(String url)  {
        // Write code here that turns the phrase above into concrete actions
        Drivers.loadApplication(getBrowser(), url, true);
    }

    @Quando("^eu preencher o campo de busca com o valor \"([^\"]*)\"$")
    public void eu_preencher_o_campo_de_busca_com_o_valor(String item)  {
        // Write code here that turns the phrase above into concrete actions
        Actions.setText(getBrowser(), By.xpath("//input[contains(@class, 'gLFyf') and contains(@class, 'gsfi')]"), item, 5);
        ExtentReports.appendToReport(getBrowser(), true);
    }

    @Quando("^dou enter no campo de busca$")
    public void dou_enter_no_campo_de_busca()  {
        // Write code here that turns the phrase above into concrete actions
        Actions.getExistingElement(getBrowser(), By.xpath("//input[contains(@class, 'gLFyf') and contains(@class, 'gsfi')]"), 5).submit();
    }

    @Entao("^devera ser apresentada a temperatura da cidade$")
    public void devera_ser_apresentada_a_temperatura_da_cidade()  {
        // Write code here that turns the phrase above into concrete actions
        Verifications.verifyElementIsVisible(getBrowser(), By.id("wob_tm"), 5, true);

        String local, diaHora, previsao, temperatura;

        local = Actions.getText(getBrowser(), By.id("wob_loc"), 5);
        diaHora = Actions.getText(getBrowser(), By.id("wob_dts"), 5);
        previsao = Actions.getText(getBrowser(), By.id("wob_dc"), 5);
        temperatura = Actions.getText(getBrowser(), By.id("wob_tm"), 5);

        System.out.println("----------------------------------------------------");
        System.out.println("Local: " + local);
        System.out.println("Dia / Hora: " + diaHora);
        System.out.println("Previsão do dia: " + previsao);
        System.out.println("Temperatura: " + temperatura + " grau(s) centígrado(s).");
        System.out.println("----------------------------------------------------");
    }

}
