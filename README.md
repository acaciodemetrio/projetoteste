# ProjetoTeste

Apenas um projeto de teste para verificar a utilização do framework da Link.


## Pré-Requisitos

Descreva os requisitos de software e hardware que é necessário para executar este projeto de automação

*   Java 1.8 SDK
*   Maven 3.5.3
*   Node.js 8.12.0
*   Appium install via node version 1.9.1
*   WinAppDriver install via node version 1.1

