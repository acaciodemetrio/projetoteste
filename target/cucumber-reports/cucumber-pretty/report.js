$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/temperatura.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "#language: pt"
    }
  ],
  "line": 2,
  "name": "Consultar temperatura em uma cidade",
  "description": "",
  "id": "consultar-temperatura-em-uma-cidade",
  "keyword": "Funcionalidade"
});
formatter.before({
  "duration": 14913232154,
  "status": "passed"
});
formatter.scenario({
  "line": 4,
  "name": "CT001 – Consultar a temperatura em uma cidade",
  "description": "",
  "id": "consultar-temperatura-em-uma-cidade;ct001-–-consultar-a-temperatura-em-uma-cidade",
  "type": "scenario",
  "keyword": "Cenario"
});
formatter.step({
  "line": 5,
  "name": "eu estou na pagina inicial do Google usando o link \"https://www.google.com.br\"",
  "keyword": "Dado "
});
formatter.step({
  "line": 6,
  "name": "eu preencher o campo de busca com o valor \"temperatura Feira de Santana\"",
  "keyword": "Quando "
});
formatter.step({
  "line": 7,
  "name": "dou enter no campo de busca",
  "keyword": "E "
});
formatter.step({
  "line": 8,
  "name": "devera ser apresentada a temperatura da cidade",
  "keyword": "Entao "
});
formatter.match({
  "arguments": [
    {
      "val": "https://www.google.com.br",
      "offset": 52
    }
  ],
  "location": "BaseSteps.eu_estou_na_pagina_inicial_do_Google_usando_o_link(String)"
});
formatter.result({
  "duration": 1900709665,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "temperatura Feira de Santana",
      "offset": 43
    }
  ],
  "location": "BaseSteps.eu_preencher_o_campo_de_busca_com_o_valor(String)"
});
formatter.result({
  "duration": 601084413,
  "status": "passed"
});
formatter.match({
  "location": "BaseSteps.dou_enter_no_campo_de_busca()"
});
formatter.result({
  "duration": 1472966487,
  "status": "passed"
});
formatter.match({
  "location": "BaseSteps.devera_ser_apresentada_a_temperatura_da_cidade()"
});
formatter.result({
  "duration": 1584631679,
  "status": "passed"
});
formatter.after({
  "duration": 1174856849,
  "status": "passed"
});
});